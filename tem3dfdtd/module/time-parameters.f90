!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    
MODULE TIME_PARAMETER
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE:: CTIME,DELT 
  !Ctime is a Nstop length array which stores the value of time of each iteration step
  ! Delt is a Nstop length array which stores the value of iteration time step.
  REAL TIME_RAMP2WAVE_SUM,TIME_RAMP2WAVE_TEMP
  ! This is used in Time_series subroutine.
ENDMODULE TIME_PARAMETER