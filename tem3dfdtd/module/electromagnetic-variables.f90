!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com

MODULE ELECTROMAGNETIC_VARIABLES
  REAL(KIND=8), DIMENSION(:,:,:), ALLOCATABLE:: EX,EY,EZ     !The x,y and z component of electric field in 3 dimensions
  REAL(KIND=8), DIMENSION(:,:,:), ALLOCATABLE:: HX,HY,HZ    !The x,y and z component of magnetic field in 3 dimensions
ENDMODULE ELECTROMAGNETIC_VARIABLES