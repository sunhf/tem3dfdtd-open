!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    
MODULE RES_MODEL_PARAMETER
  REAL(KIND=8), DIMENSION(:,:,:), ALLOCATABLE:: CCSIG    !The conductivity in each grid
ENDMODULE RES_MODEL_PARAMETER