!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
!this subroutine is writen by Huaifeng Sun from May 29, 2017

subroutine getxmldata
    use constantparameters
    use ifport
    implicit none
    logical alive
    integer temp_ii,iii
    !this following lines 10-21 are added by huaifeng sun to get the dir 2016-10-30
    character(255) dir
    character(255) inputfilename
    integer(4) length
    length = getdrivedirqq(dir)
    if (length .gt. 0) then
        inputfilename=trim(dir)//'//example//input.dat'
    else
        write (*,*) 'failed to get current directory'
        pause
    end if
    !the following inputfilename type are modified by hfsun 2016-10-30
    inquire(file=inputfilename, exist=alive)
    if(.not. alive) then
        write(10005,*) "input.dat does not exist."
        write (*,*)"input.dat does not exist."
        pause
    else
        !the following starts to read the xml data file.

    endif
  
  
endsubroutine getxmldata
    