!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    
subroutine WriteRecFiles(num)
  use constantparameters
  use electromagnetic_variables
  use time_parameter
  implicit none
  integer ii,jj,num
  select case(RecFlag)
  case('Hz')
     call SubWriteRecFiles('Hz',num)
  case('HE')
     call SubWriteRecFiles('HE',num)
  end select
end subroutine WriteRecFiles
