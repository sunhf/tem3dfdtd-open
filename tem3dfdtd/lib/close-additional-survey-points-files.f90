!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    
!function description
    !this suboutine is used to close #5300 file.
    !2016-10-30

SUBROUTINE CLOSE_ADDTIONAL_SURVERY_POINTS_FILES
  CLOSE(5300)
ENDSUBROUTINE CLOSE_ADDTIONAL_SURVERY_POINTS_FILES
