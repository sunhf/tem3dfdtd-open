!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    
subroutine OpenRecFiles
  ! This subroutine opens all the files needed to record data of interests
  use constantparameters
  implicit none
  integer ii,jj
  PostProcessFilePid=19999; SplitFilePid=19998
  PostProcessFile='PostProcessFileList.dat'; SplitFile='Split.dat'
  ! This file is used to dominate the post-process program which records all the filenames needed to be processed and some other parameters.
  open(PostProcessFilePid,file=PostProcessFile)
  open(SplitFilePid,file=SplitFile)
  select case(RecFlag)
  case('HE')
     call SubOpenRecFiles('HE')
  case('Hz')
     call SubOpenRecFiles('Hz')
  end select
  write(PostProcessFilePid,'(e12.6e2)')raisetime+wave+ramp
  close(PostProcessFilePid)
end subroutine OpenRecFiles

