!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    
!function description
    !this suboutine is used to select and call some suboutine to close some 
    !selected files.
    !2016-10-30
    
subroutine CloseRecFiles
  use constantparameters
  implicit none
  integer ii,jj
  select case(RecFlag)
  case('Hz')
     call SubCloseRecFiles('Hz')
  case('HE')
     call SubCloseRecFiles('HE')
  end select
end subroutine CloseRecFiles
