!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    
SUBROUTINE RES_CONFIGURE
  !本子程序用于设置模型的电阻率参数
  USE CONSTANTPARAMETERS
  USE ELECTROMAGNETIC_VARIABLES
  USE RES_MODEL_PARAMETER
  USE TIME_PARAMETER
  USE OMP_LIB
  IMPLICIT NONE
  INTEGER II,III,i,j,k
  DO K=1,NZ
     DO J=1,NY
        DO I=1,NX
           CCSIG(I,J,K)=BACKGROUND_CONDUCTIVITY !Set the background value of conductivity.
        ENDDO
     ENDDO
  ENDDO
  II=SIZE(TAR_X1)
  DO III=1,II
     DO K=TAR_Z1(III),TAR_Z2(III)
        DO J=TAR_Y1(III),TAR_Y2(III)
           DO I=TAR_X1(III),TAR_X2(III)
              CCSIG(I,J,K)=TAR_CONDUCTIVITY(III) !Set the value of anomalous conductivity.
           ENDDO
        ENDDO
     ENDDO
  ENDDO
  SIGMA_MIN=MINVAL(CCSIG)
  RETURN
ENDSUBROUTINE RES_CONFIGURE
!------------------------
