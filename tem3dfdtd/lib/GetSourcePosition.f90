!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    
subroutine GetSourcePosition
  ! This subroutine initialize the value of array is_ex_in_source which is used when judging whether the grid contains the source
  ! For the modification of OpenACC code.
  use constantparameters
  IMPLICIT NONE
  INTEGER ii,jj
  is_ex_in_source=0; is_ey_in_source=0
  do ii=nxs-(SourceGridNum-1)/2,nxs+(SourceGridNum-1)/2,1
     is_ex_in_source(ii,nys-(SourceGridNum-1)/2)=1
     is_ex_in_source(ii,nys+(SourceGridNum+1)/2)=-1
  end do
  ! Aware that the value of source has both positive and negative parts, or they will cancel each other out.
  do ii=nys-(SourceGridNum-1)/2,nys+(SourceGridNum-1)/2,1
     is_ey_in_source(nxs-(SourceGridNum-1)/2,ii)=-1
     is_ey_in_source(nxs+(SourceGridNum+1)/2,ii)=1
  end do
end subroutine GetSourcePosition

