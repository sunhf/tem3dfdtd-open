!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    
subroutine Get_mstop
  ! if the value of plus is too small, it will cause array bounds exceeded because the array bonds of Mstop and Mstart is set to 10000.
  ! If you do want to set a series of small plus, you should change the bounds of Mstop and Mstart in Module Constantparameters.---------------luxushan
  use constantparameters
  use time_parameter
  implicit none
  integer:: ii,jj,dt,plus,plusMid
  ! ----------------------------------------------------------------------------------------------------------------------------------!
  ii=1; num_fra_com=1; plus=50  !Plus denotes the number of iteration steps in each computing fraction.
  do while(ctime(ii).lt.raisetime) !This is the raising edge of trapeziodal waveform
     mstop(num_fra_com)=plus; mstart(num_fra_com)=ii
     ii=ii+plus;   num_fra_com=num_fra_com+1
  enddo
  ! -----------------------------------------------------------------------------------------------------------------------------------!
  ! The value of raisetime and raisestep have been set to 1e-6 and 1e-9 for centries so that you are supposed to set plus at a value that mod(1000, plus).eq.0
  ! ------------------------------------------------------------------------------------------------------------------------------------!
  plus=2500                     !The duration period is very long compared with the entire computation time so that the value of Plus is set to a comparatively large--
  ! --value so that you don't have to record too much useless values of EM field in duration period.
  do while(ctime(ii).le.wave+raisetime)
     mstop(num_fra_com)=plus; mstart(num_fra_com)=ii
     ii=ii+plus;    num_fra_com=num_fra_com+1
     if(ctime(ii).gt.wave+raisetime)then
        ii=ii-plus; num_fra_com=num_fra_com-1; jj=ii
        do while(ctime(jj).le.raisetime+wave)
           jj=jj+1
        enddo
        plus=jj-ii
        mstop(num_fra_com)=plus; mstart(num_fra_com)=ii
        ii=ii+plus; num_fra_com=num_fra_com+1;
     endif
  enddo
  ! -------------------------------------------------------------------------------------------------------------------------------------!
  ! -------------------------------------------------------------------------------------------------------------------------------------!
  plus=10                       !Sometimes the code diverges at the ramp time, so a comparatively small value is set to observe the divergence process
  do while(ctime(ii).le.wave+raisetime+ramp)
     mstop(num_fra_com)=plus; mstart(num_fra_com)=ii
     ii=ii+plus; num_fra_com=num_fra_com+1
     if(ctime(ii).gt.wave+raisetime+ramp)then
        ii=ii-plus; num_fra_com=num_fra_com-1; jj=ii
        do while(ctime(jj).le.raisetime+wave+ramp)
           jj=jj+1
        enddo
        plus=jj-ii
        mstop(num_fra_com)=plus; mstart(num_fra_com)=ii
        ii=ii+plus; num_fra_com=num_fra_com+1;
     endif
  enddo
  ! -------------------------------------------------------------------------------------------------------------------------------------!
  ! -------------------------------------------------------------------------------------------------------------------------------------!
  ! In this part, plus keeps increasing because there seems to be no need to make a dense record in the late time of TEM problems---
  ! --and actually the instruments performs similar recording strategy, however, in a equal logarithm manner.
  plus=10
  mstop(num_fra_com)=plus;  mstart(num_fra_com)=ii
  num_fra_com=num_fra_com+1;  ii=ii+plus
  do while(ii.le.nstop)
     plusMid=plus*1.1
     if((plusMid-plus).le.1)then
        plus=plus+1
     else
        plus=plusMid
     end if
     if(plus.ge.100)then
        plus=100
     end if
     mstop(num_fra_com)=plus; mstart(num_fra_com)=ii
     num_fra_com=num_fra_com+1
     ii=ii+plus
     if(ii.gt.nstop)then
        ii=ii-plus; num_fra_com=num_fra_com-1; jj=ii
        do while(jj.le.nstop)
           jj=jj+1
        enddo
        plus=jj-ii
        mstop(num_fra_com)=plus; mstart(num_fra_com)=ii
        ii=ii+plus; num_fra_com=num_fra_com+1
     endif
  enddo
  ! ----------------------------end of distribution-----------------------------------------!
end subroutine Get_mstop 

!--------------------------------------------------------------------------------------------------!


