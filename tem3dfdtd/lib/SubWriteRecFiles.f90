!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    
subroutine SubWriteRecFiles(Flag,num)
  ! This subroutine writes all the data of intersted recording points from different recording plane which are given in the input.dat file.
  use constantparameters
  use electromagnetic_variables
  use time_parameter
  implicit none

  integer ii,jj,kk,num
  character*2 Flag
  character*3 string
  real*8 TmpHx(NumRecLines,NumRecPoints,NumRecHeights+1),TmpHy(NumRecLines,NumRecPoints,NumRecHeights+1),TmpHz(NumRecLines,NumRecPoints,NumRecHeights+1),&
       &TmpEx(NumRecLines,NumRecPoints,NumRecHeights+1),TmpEy(NumRecLines,NumRecPoints,NumRecHeights+1),TmpEz(NumRecLines,NumRecPoints,NumRecHeights+1)
  ! -------------------------------------------case selection---------------------------------------------!
  select case(Flag)
  case('Hz')
     RecFilePid=RecHzFilePid
     do ii=1,NumRecLines,1
        do jj=RecPointMin,RecPointMax,1
           TmpHz(ii,jj-RecPointMin+1,1)=Hz(RecLine(ii),jj-RecPointMin+1,Nz/2+1)
        end do
     end do
     do jj=1,NumRecLines,1
        do ii=RecPointMin,RecPointMax,1
           do kk=1,NumRecHeights,1
              TmpHz(ii-RecPointMin+1,jj,kk+1)=Hz(RecLine(ii),jj,Nzs_air(kk))
           end do
        end do
     end do
     do ii=1,NumRecLines,1
        do jj=RecPointMin,RecPointMax,1
           write(string,'(I3.3)')jj
           write(RecFilePid(1,ii),'(a3,2e20.10e3)')string,Ctime(mstart(num)+mstop(num)-1),TmpHz(RecLine(ii),jj-RecPointMin+1,1)
           do kk=1,NumRecHeights,1
              write(RecFilePid(kk+1,ii),'(a3,2e20.10e3)')string,Ctime(mstart(num)+mstop(num)-1),TmpHz(RecLine(ii),jj-RecPointMin+1,kk+1)
           end do
        end do
     end do
  case('HE')
     RecFilePid=RecHEFilePid
     do ii=1,NumRecLines,1
        do jj=RecPointMin,RecPointMax,1
           TmpEx(ii,jj-RecPointMin+1,1)=Ex(RecLine(ii),jj,Nz/2+1)
           TmpEy(ii,jj-RecPointMin+1,1)=Ey(RecLine(ii),jj,Nz/2+1)
           TmpEz(ii,jj-RecPointMin+1,1)=Ez(RecLine(ii),jj,Nz/2+1)
           TmpHx(ii,jj-RecPointMin+1,1)=Hx(RecLine(ii),jj,Nz/2+1)
           TmpHy(ii,jj-RecPointMin+1,1)=Hy(RecLine(ii),jj,Nz/2+1)
           TmpHz(ii,jj-RecPointMin+1,1)=Hz(RecLine(ii),jj,Nz/2+1)
        end do
     end do
     do ii=1,NumRecLines,1
        do jj=RecPointMin,RecPointMax,1
           do kk=1,NumRecHeights,1
              TmpHx(ii,jj-RecPointMin+1,kk+1)=Hx(RecLine(ii),jj,Nzs_air(kk)+1)
              TmpHy(ii,jj-RecPointMin+1,kk+1)=Hy(RecLine(ii),jj,Nzs_air(kk)+1)
              TmpHz(ii,jj-RecPointMin+1,kk+1)=Hz(RecLine(ii),jj,Nzs_air(kk)+1)
              TmpEx(ii,jj-RecPointMin+1,kk+1)=Ex(RecLine(ii),jj,Nzs_air(kk)+1)
              TmpEy(ii,jj-RecPointMin+1,kk+1)=Ey(RecLine(ii),jj,Nzs_air(kk)+1)
              TmpEz(ii,jj-RecPointMin+1,kk+1)=Ez(RecLine(ii),jj,Nzs_air(kk)+1)
           end do
        end do
     end do
     do ii=1,NumRecLines,1
        do jj=RecPointMin,RecPointMax,1
           write(string,'(I3.3)')jj
           write(RecFilePid(1,ii),'(a3,7e20.10e3)')string,Ctime(mstart(num)+mstop(num)-1),TmpHx(ii,jj-RecPointMin+1,1),TmpHy(ii,jj-RecPointMin+1,1),TmpHz(ii,jj-RecPointMin+1,1),&
                &TmpEx(ii,jj-RecPointMin+1,1),TmpEy(ii,jj-RecPointMin+1,1),TmpEz(ii,jj-RecPointMin+1,1)
           do kk=1,NumRecHeights,1
              write(RecFilePid(kk+1,ii),'(a3,7e20.10e3)')string,Ctime(mstart(num)+mstop(num)-1),TmpHx(ii,jj-RecPointMin+1,kk+1),TmpHy(ii,jj-RecPointMin+1,kk+1),TmpHz(ii,jj-RecPointMin+1,kk+1),&
                   &TmpEx(ii,jj-RecPointMin+1,kk+1),TmpEy(ii,jj-RecPointMin+1,kk+1),TmpEz(ii,jj-RecPointMin+1,kk+1)
           end do
        end do
     end do
  end select
  ! ------------------------------------------------------end case selection---------------------------------------------!
end subroutine SubWriteRecFiles



