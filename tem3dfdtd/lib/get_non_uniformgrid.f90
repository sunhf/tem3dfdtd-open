!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    
SUBROUTINE GET_NON_UNIFORMGRID
  USE CONSTANTPARAMETERS
  USE OMP_LIB
  IMPLICIT NONE
  INTEGER II
  INTEGER MID_P,LEFT_P,RIGHT_P,UP_P,DOWN_P
  REAL(KIND=8) CDELX_LENGTH,CDELY_LENGTH,CDELZ_LENGTH
  ! -------------------------mesh-z-------------------------------------------!
  Coordiz3(nzs)=-GridSize; Coordiz3(nzs+1)=0
  do ii=nzs-20,nzs+20,1
     Cdelz(ii)=GridSize
  end do                        !Uniform mesh in an area equal to source length
  do ii=nzs-21,1,-1
     Cdelz(ii)=Cdelz(ii+1)*scale_par
     if(Cdelz(ii).gt.200)then
        Cdelz(ii)=200
     end if
  end do                        !Ununiform mesh in the air.
  do ii=nzs+21,nz,1
     Cdelz(ii)=Cdelz(ii-1)*scale_par
     if(Cdelz(ii).gt.200)then
        Cdelz(ii)=200
     end if
  end do                        !Ununiform mesh underground
  do ii=nzs-1,1,-1
     Coordiz3(ii)=Coordiz3(ii+1)-Cdelz(ii)
  end do
  do ii=nzs+2,nz,1
     Coordiz3(ii)=Coordiz3(ii-1)+Cdelz(ii)
  end do                        !Record the coordination information of each grid.
  ! ----------------------------end of mesh------------------------------------!
  ! -------------------------------mesh x----------------------------------------!
    if(SourceLength/GridSize.gt.51)then
     do ii=nxs-(SourceLength/GridSize-1)/2,nxs+(SourceLength/GridSize-1)/2,1
        Cdelx(ii)=GridSize
     end do
     do ii=nxs-(SourceLength/GridSize-1)/2-1,1,-1
        Cdelx(ii)=Cdelx(ii+1)*scale_par
        if(Cdelx(ii).gt.200)then
           Cdelx(ii)=200
        end if
     end do
     do ii=nxs+(SourceLength/GridSize-1)/2+1,nx,1
        Cdelx(ii)=Cdelx(ii-1)*scale_par
        if(Cdelx(ii).gt.200)then
           Cdelx(ii)=200
        end if
     end do
  else 
     do ii=nxs-50,nxs+50,1
        Cdelx(ii)=GridSize
     end do
     do ii=nxs-51,1,-1
        Cdelx(ii)=Cdelx(ii+1)*scale_par
        if(Cdelx(ii).gt.200)then
           Cdelx(ii)=200
        end if
     end do
     do ii=nxs+51,nx,1
        Cdelx(ii)=Cdelx(ii-1)*scale_par
        if(Cdelx(ii).gt.200)then
           Cdelx(ii)=200
        endif
     end do
  end if
  Coordix3(nxs)=0
  do ii=nxs-1,1,-1
     Coordix3(ii)=Coordix3(ii+1)-(Cdelx(ii)+Cdelx(ii+1))/2
  end do
  do ii=nxs+1,nx,1
     Coordix3(ii)=Coordix3(ii-1)+(Cdelx(ii-1)+Cdelx(ii))/2
  end do
  ! -----------------------------end of mesh------------------------------------!
  ! --------------------------------mesh y----------------------------------------!
  if(SourceLength/GridSize.gt.51)then
     do ii=nys-(SourceLength/GridSize-1)/2,nys+(SourceLength/GridSize-1)/2,1
        Cdely(ii)=GridSize
     enddo
     do ii=nys-(SourceLength/GridSize-1)/2-1,1,-1
        Cdely(ii)=Cdely(ii+1)*scale_par
        if(Cdely(ii).gt.200)then
           Cdely(ii)=200
        end if
     end do
     do ii=nys+(SourceLength/GridSize-1)/2+1,ny,1
        Cdely(ii)=Cdely(ii-1)*scale_par
        if(Cdely(ii).gt.200)then
           Cdely(ii)=200
        endif
     end do
  else 
     do ii=nys-25,nys+25,1
        Cdely(ii)=GridSize
     end do
     do ii=nys-26,1,-1
        Cdely(ii)=Cdely(ii+1)*scale_par
        if(Cdely(ii).gt.200)then
           Cdely(ii)=200
        end if
     end do
     do ii=nys+26,ny,1
        Cdely(ii)=Cdely(ii-1)*scale_par
        if(Cdely(ii).gt.200)then
           Cdely(ii)=200
        end if
     end do
  end if
  Coordiy3(nys)=-(GridSize/2); Coordiy3(nys+1)=GridSize/2
  do ii=nys-1,1,-1
     Coordiy3(ii)=Coordiy3(ii+1)-(Cdely(ii)+Cdely(ii+1))/2
  end do
  do ii=nys+1,ny,1
     Coordiy3(ii)=Coordiy3(ii-1)+(Cdely(ii)+Cdely(ii-1))/2
  end do
  ! ------------------------------end of mesh-----------------------------------!
  ! ------------------------------record coordinate----------------------------!
  open(10006,file='HzCoordinate.dat') !You can find the coordination information of each grid in this file.
  write(10006,*)nx,ny,nz
  write(10006,*)'!---------------------------------X part--------------------------------!'
  do ii=1,nx,1
     write(10006,*)ii,Coordix3(ii)
  end do
  write(10006,*)'!----------------------------end of X part----------------------------!'
  write(10006,*)'!---------------------------------Y part---------------------------------!'
  do ii=1,ny,1
     write(10006,*)ii,Coordiy3(ii)
  end do
  write(10006,*)'!----------------------------end of Y part----------------------------!'
  write(10006,*)'!---------------------------------Z part---------------------------------!'
  do ii=1,nz,1
     write(10006,*)ii,Coordiz3(ii)
  end do
  write(10006,*)'!----------------------------end of Z part----------------------------!'
  close(10006)
  !----------------------------end of recording-------------------------------!

  CDELX_LENGTH=SUM(CDELX)
  CDELY_LENGTH=SUM(CDELY)
  CDELZ_LENGTH=SUM(CDELZ)
  WRITE(10005,*)'设置的模型尺寸为：'
  WRITE(10005,*)'SUM_X=',CDELX_LENGTH
  WRITE(10005,*)'SUM_Y=',CDELY_LENGTH
  WRITE(10005,*)'SUM_Z=',CDELZ_LENGTH

  WRITE(10005,*)'相邻网格放大系数=',SCALE_PAR
  WRITE(10005,*)'最大网格尺寸与最小网格尺寸之比<=',MAX_RATIO
  WRITE(10005,*)'X方向的非均匀网格尺寸为：'
  WRITE(10005,'(5F18.8)')CDELX

  WRITE(10005,*)'Y方向的非均匀网格尺寸为：'
  WRITE(10005,'(5F18.8)')CDELY
  WRITE(10005,*)'Z方向的非均匀网格尺寸为：'
  WRITE(10005,'(5F18.8)')CDELZ

  WRITE(*,*)'Model size:',CDELX_LENGTH,CDELY_LENGTH,CDELZ_LENGTH

  OPEN(400,FILE='CDELX.DAT',STATUS='UNKNOWN')
  DO II=1,NX
     WRITE(400,'(E13.6)')CDELX(II)
  ENDDO
  CLOSE(400)
  OPEN(400,FILE='CDELY.DAT',STATUS='UNKNOWN')
  DO II=1,NY
     WRITE(400,'(E13.6)')CDELY(II)
  ENDDO
  CLOSE(400)
  OPEN(400,FILE='CDELZ.DAT',STATUS='UNKNOWN')
  DO II=1,NZ
     WRITE(400,'(E13.6)')CDELZ(II)
  ENDDO
  CLOSE(400)
ENDSUBROUTINE GET_NON_UNIFORMGRID
