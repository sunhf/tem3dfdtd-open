!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    
!function description
    !this suboutine is used to write out the readed calculation parameters for 
    !errors or mistakes check.
    !2016-10-30
    
SUBROUTINE CHECKPARAMETERS
  USE CONSTANTPARAMETERS
  IMPLICIT NONE
  integer i
  WRITE(10005,*)'请检查计算参数:    '
  WRITE(10005,*)'矩形回线边长为：',SourceLength
  WRITE(10005,*)'X,Y,Z方向的网格数分别为：',NX,NY,NZ
  WRITE(10005,*)'线圈中心所处的网格为：   ',NXS,NYS,NZS
  WRITE(10005,*)'输入的最大迭代次数为：         ',NSTOP
  WRITE(10005,*)
  WRITE(10005,*)'X,Y,Z方向最小晶格尺寸分别为：'
  WRITE(10005,*)'DELTA_X=',GridSize
  WRITE(10005,*)'DELTA_Y=',GridSize
  WRITE(10005,*)'DELTA_Z=',GridSize
  WRITE(10005,*)'背景电导率',BACKGROUND_CONDUCTIVITY
  WRITE(10005,*)
  WRITE(10005,*)'异常体参数'
  WRITE(10005,*)'NO    X1  X2    Y1  Y2   Z1   Z2    CONDUCTIVITY'
  DO I=1,SIZE(TAR_X1)        
     WRITE(10005,'(I3,6I5,ES15.6)')I,TAR_X1(I),TAR_X2(I),TAR_Y1(I),TAR_Y2(I),TAR_Z1(I),TAR_Z2(I),TAR_CONDUCTIVITY(I)
  ENDDO
  RETURN
ENDSUBROUTINE CHECKPARAMETERS
