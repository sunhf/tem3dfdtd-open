!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    
SUBROUTINE GET_SYS_TIMEDATA(OUTPUT)
  ! The original subroutien written by Huaifeng Sun can not work in PGI compiler, so I change it into what it looks like here.
  IMPLICIT NONE
  CHARACTER*4 TEMP1,TEMP2,TEMP3,TEMP4,TEMP5,TEMP6
  CHARACTER*20 OUTPUT
  integer*4 FortranDate(3),FortranTime(3)
  INTEGER(4) TMPDAY, TMPMONTH, TMPYEAR
  INTEGER(4) TMPHOUR, TMPMINUTE, TMPSECOND
  
  !the following code is optimized by hfsun@2017-5-29 to modify an warning on the use of idate
  !I also replace the function idate with idate4 to get a 4 digital year.
  !But I received errors when use CALL idate4(FortranDate), so I use the temp solution idate4(tmpmonth,tmpday,tmpyear)
     
  !CALL idate4(FortranDate)
  CALL itime(FortranTime)
  !tmpday=FortranDate(2); tmpmonth=FortranDate(1); tmpyear=FortranDate(3)
  tmphour=FortranTime(1); tmpminute=FortranTime(2); tmpsecond=FortranTime(3)
  CALL idate4(tmpmonth,tmpday,tmpyear)
  !CALL itime(tmphour,tmpminute,tmpsecond)
  !tmpday=FortranDate(2); tmpmonth=FortranDate(1); tmpyear=FortranDate(3)
  !tmphour=FortranTime(1); tmpminute=FortranTime(2); tmpsecond=FortranTime(3)
  WRITE(TEMP1,'(I4)')TMPYEAR
  WRITE(TEMP2,'(I2)')TMPMONTH
  WRITE(TEMP3,'(I2)')TMPDAY
  WRITE(TEMP4,'(I4)')TMPHOUR
  WRITE(TEMP5,'(I4)')TMPMINUTE
  WRITE(TEMP6,'(I4)')TMPSECOND
  OUTPUT=TRIM(ADJUSTL(TEMP1))//'-'//TRIM(ADJUSTL(TEMP2))//'-'//TRIM(ADJUSTL(TEMP3))//'  '//TRIM(ADJUSTL(TEMP4))//':'//TRIM(ADJUSTL(TEMP5))//':'//TRIM(ADJUSTL(TEMP6))
  OUTPUT=TRIM(ADJUSTL(OUTPUT))
  RETURN
ENDSUBROUTINE GET_SYS_TIMEDATA
