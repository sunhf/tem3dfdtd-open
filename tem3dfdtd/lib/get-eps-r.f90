!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com

subroutine Get_eps_r
  use constantparameters
  use time_parameter
  implicit none
  integer ii
  do ii=1,nstop,1
     EPS_R(ii)=3.0D0*(DELT(ii)/GridSize)**2/MU0
     Cq(ii)=(DELT(ii-1)+DELT(ii))/(2.0D0*MU0)
     ! It is needed in the iterative subroutine, and we don't have to compute it in each iterative process.
  enddo
end subroutine Get_eps_r


