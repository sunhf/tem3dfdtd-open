!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    
SUBROUTINE MEMORY_USE_ESTIMATION 
  !This subroutine is written by Huaifeng Sun, and it has not been modified since the last ice age, so it can not reveal the real consumption of memory now.
  ! This subroutine needs further modification. You can, you do!
  USE CONSTANTPARAMETERS
  IMPLICIT NONE
  INTEGER(KIND=8) TUSE
  CHARACTER(LEN=40) XSTRING
  INTEGER(KIND=1) CONTD
  TUSE=0.0
  TUSE=TUSE+NX*NYB*NZB+NXB*NY*NZB+NXB*NYB*NZ      !电场E使用内存
  TUSE=TUSE+NXB*NY*(NZ+1)+NX*NYB*(NZ+1)+NX*NY*NZB !磁场H使用内存
  TUSE=TUSE+4*NY*NZB+4*NYB*NZ+NX*4*NZB+NXB*4*NZ+NX*NYB*4+NXB*NY*4 !边界条件使用内存
  TUSE=TUSE+4*NY*NZB+4*NYB*NZ+NX*4*NZB+NXB*4*NZ+NX*NYB*4+NXB*NY*4 !边界条件使用内存
  TUSE=TUSE+NXB*NYB*NZB   !模型使用内存
  TUSE=TUSE+NSTOP*2
  TUSE=TUSE/1024
  TUSE=TUSE/1024
  TUSE=TUSE*16
  WRITE (XSTRING,'(I40)') TUSE
  XSTRING = 'At least '//TRIM(ADJUSTL(XSTRING))//'M memory is needed!' !拼接为要求的FORMAT格式
  XSTRING = TRIM(ADJUSTL(XSTRING)) 
  WRITE(*,*)XSTRING
  RETURN
ENDSUBROUTINE MEMORY_USE_ESTIMATION
