!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    
SUBROUTINE ZERO
  USE CONSTANTPARAMETERS
  USE ELECTROMAGNETIC_VARIABLES
  USE RES_MODEL_PARAMETER
  USE TIME_PARAMETER
  USE OMP_LIB
  !本子程序将计算中的数组赋0值进行初始化
  IMPLICIT NONE
  CCSIG=0.0D0 
  EX=0.0D0
  EY=0.0D0     
  EZ=0.0D0      
  HX=0.0D0
  HY=0.0D0
  HZ=0.0D0  
  RETURN
ENDSUBROUTINE ZERO