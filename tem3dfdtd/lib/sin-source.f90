!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    

SUBROUTINE SIN_SOURCE
  USE CONSTANTPARAMETERS
  USE TIME_PARAMETER
  USE OMP_LIB
  IMPLICIT NONE
  integer i,j,k
  DELT=1.0D-7
  CTIME(1)=0.0D0
  SOURCE(1)=0.0D0
  DO I=2,NSTOP
     CTIME(I)=CTIME(I-1)+DELT(I-1)
     IF(CTIME(I) .LT. WAVE)THEN
        SOURCE(I)=AMP*SIN(PI*CTIME(I)/WAVE)
     ELSE
        SOURCE(I)=0.0D0
     ENDIF
  ENDDO
  OPEN(9,FILE='CTIME_SIN_SOURCE.DAT',STATUS='UNKNOWN')
  DO I=1,NSTOP
     WRITE(9,'(3E24.16)')CTIME(I),DELT(I),SOURCE(I)
  ENDDO
  CLOSE(9)
  WRITE(10005,*)'半正弦发射波形时间序列已经写入文件CTIME_SIN_SOURCE.DAT'
  RETURN
ENDSUBROUTINE SIN_SOURCE
