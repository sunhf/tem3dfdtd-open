!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    
subroutine SubCloseRecFiles(Flag)
  use constantparameters
  implicit none
  character*2 Flag
  integer ii,jj
  select case(Flag)
  case('Hz')
     RecFilePid=RecHzFilePid
  case('HE')
     RecFilePid=RecHEFilePid
  end select
  do ii=1,NumRecHeights,1
     do jj=1,NumRecLines,1
        close(RecFilePid(ii,jj))
     end do
  end do
end subroutine SubCloseRecFiles
