!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com
    
SUBROUTINE TRIANGLE_SOURCE
  USE CONSTANTPARAMETERS
  USE TIME_PARAMETER
  USE OMP_LIB
  IMPLICIT NONE
  integer i,j,k
  DELT=1.0D-7
  CTIME(1)=0.0D0
  SOURCE(1)=0.0D0
  DO I=2,NSTOP
     CTIME(I)=CTIME(I-1)+DELT(I-1)
     IF(CTIME(I) .LT. WAVE/2.0D0)THEN
        SOURCE(I)=2*AMP*CTIME(I)/WAVE
     ELSEIF (CTIME(I) .GE. WAVE/2.0D0 .AND. CTIME(I) .LE. WAVE) THEN
        SOURCE(I)=-2*AMP*CTIME(I)/WAVE+2*AMP
     ELSE
        SOURCE(I)=0.0D0
     ENDIF
  ENDDO
  OPEN(9,FILE='CTIME_TRIANGLE_SOURCE.DAT',STATUS='UNKNOWN')
  DO I=1,NSTOP
     WRITE(9,'(3E24.16)')CTIME(I),DELT(I),SOURCE(I)
  ENDDO
  CLOSE(9)
  WRITE(10005,*)'三角发射波形时间序列已经写入文件CTIME_TRIANGLE_SOURCE.DAT'
  RETURN
ENDSUBROUTINE TRIANGLE_SOURCE
