!Copyright (c) 2013 by tdem.org under guide of Xiu Li(lixiu@chd.edu.cn)
!written by Huaifeng Sun(sunhuaifeng@gmail.com) and Xushan Lu(luxushan@gmail.com)
!Code distribution @ tdem.org or sunhuaifeng.com

! This is a finite difference time domain (FDTD) code for the simulation of transient electromagnetic (TEM);
! This code is designed to  be used in semi_airborne TEM with a loop source;
! This code is written by Huaifeng Sun (sunhuaifeng@gmail.com) and Xushan Lu (luxushan@gmail.com);
! OpenACC API is used in this code for the acceleration with GPU device; therefore, you are recommended to compile this code with --
! --PGI Accelerator Fortran Workstation compiler. A Nvidia GPU card with CUDA capability is required if you want to run this code in parallel mode.
! Nobody is allowed to copy or distribute this code to people outside of TDEM.org group without the permission from Prof. Xiu Li (lixiu@chd.edu.cn)--
! --or you will be 
! Contact the author for more detailed information.

!------------------------------------------------Instruction part--------------------------------------------------!
! This module is used to declare most of the parameters which are used in the entire code.
!-----------------------------------------------------------------------------------------------------------------------! 
!==========================������ʼ==============================
PROGRAM MAIN
  USE OMP_LIB
  USE CONSTANTPARAMETERS
  USE ELECTROMAGNETIC_VARIABLES
  USE RES_MODEL_PARAMETER
  USE TIME_PARAMETER
  IMPLICIT none
  CHARACTER*20, XSTRING
  CHARACTER*20, SYS_TIME 
  OPEN(10005,FILE='logfile.log',STATUS='UNKNOWN')
  CALL GET_SYS_TIMEDATA(SYS_TIME)
  WRITE(10005,*)'----------------------',SYS_TIME,'----------------------'
  CALL GETDATA                  !This subroutine is used to input all the needed parameter of each calculation from 'input.dat' file.
  CALL CHECKPARAMETERS          !This subroutine is used to chech the correctness of input
  WRITE (XSTRING,'(I3)') NX
  XSTRING = '('//TRIM(ADJUSTL(XSTRING))//'E28.16E3)' 
  XSTRING = TRIM(ADJUSTL(XSTRING)) 
  CALL MEMORY_USE_ESTIMATION  !This subroutine is used to estimate the total memory usage according to the input,
  CALL ALLOCATEMEMORY         !This subroutine is used to allocate the memory in Host.
  WRITE(*,*)'Preparing the non-uniform grid.. .. .. ..'
  CALL GET_NON_UNIFORMGRID    !This subroutine is used to mesh the non-uniform grid model.
  WRITE(*,*)'Initializing the parameters.. .. ..'
  CALL ZERO                     !This subroutine is used to initialize the value of array.
  WRITE(*,*)'Creating resistivity model.. .. ..'
  CALL RES_CONFIGURE            !This subroutine is used to distribute the resistivity (or conductivity) of the geology model to each grid
  WRITE(*,*)'Creating computing time series.. .. ..'
  CALL TIME_SERIOUS             !This subroutine is used to creat the time series of the entire computation
  WRITE(*,*)'Preparing array receiver points.. .. ..'
  WRITE(*,*)'Starting computing.. .. ..'
  CALL GET_SYS_TIMEDATA(SYS_TIME)
  WRITE(10005,*)'----------------------',SYS_TIME,'----------------------'
  call Get_eps_r                !This subroutine is used to get the fictitious dielectric constant
  call Get_mstop                !This subroutine is used to cut the entire computation process into computation fractions
  call GetSourcePosition        !This subroutine is used to get the source position in the model.
  call OpenRecFiles             !This subroutine is used to open all the files for the record of simulation data.
  call Iteration                !This subroutine is the iteration subroutine of EM filed
  call CloseRecFiles            !This subroutine is used to close all the opened recording files
  CALL FREE_MEMORY         !This subroutine is used to deallocate all the memory allocated before iteration
  CALL GET_SYS_TIMEDATA(SYS_TIME)
  WRITE(10005,*)'----------------------',SYS_TIME,'----------------------'
  WRITE(10005,*)'Computation finished��'
  CLOSE(10005)
END PROGRAM MAIN

